﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pisca : MonoBehaviour
{
    public GameObject game;
    [SerializeField] bool pulsa = false;
    public void Start()
    {
        Debug.LogWarning("entrou");

        pulsa = true;
        
    }
    private void OnMouseEnter()
    {
        Debug.LogWarning("entrou");
        if (pulsa)
        {
            startpulse();
        }
    }
    
    public void OnMouseOver()
    {
        Debug.Log("entrou2");
        if (pulsa)
        {
            Debug.Log("Entrou no if do over");
            //startpulse();
            StartCoroutine(startpulse());
        }   
    }
    public IEnumerator startpulse()
    {
        Debug.LogWarning("Entrou no startpulse");
        
        for (float i = 0f; i <= 1f; i += 0.1f)
        {
            transform.localScale = new Vector2(Mathf.Lerp(transform.localScale.x, transform.localScale.x + 0.055f, Mathf.SmoothStep(0f, 1f, i)),
                (Mathf.Lerp(transform.localScale.y, transform.localScale.y + 0.055f, Mathf.SmoothStep(0f, 1f, i))));
            yield return new WaitForSeconds(0.015f);
        }
        for (float i = 0f; i <= 1f; i += 0.1f)
        {
           transform.localScale = new Vector2(
                Mathf.Lerp(transform.localScale.x, transform.localScale.x - 0.055f, Mathf.SmoothStep(0f, 1f, i)),
                (Mathf.Lerp(transform.localScale.y, transform.localScale.y - 0.055f, Mathf.SmoothStep(0f, 1f, i))));
            yield return new WaitForSeconds(0.015f);
        }

    }
}
